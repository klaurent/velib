package com.velib.services

import scala.concurrent.{ ExecutionContext, Future }

import com.velib.model.{ Error, VelibData }
import com.velib.utils.Logging
import play.api.Configuration
import play.api.libs.ws.WSClient

class OpenDataClient(
  ws: WSClient,
  configuration: Configuration
)(implicit ec:ExecutionContext) extends Logging{
  private val url = configuration.get[String]("opendata.url")

  def findVelibData(): Future[Either[Error, VelibData]] = ws.url(url).withMethod("GET").get().map { response =>
    if (response.status == 200) {
      VelibData.format.reads(response.json).asEither match {
        case Left(e) => {
          Left(Error(e.toString()))
        }
        case Right(v) => Right(v)
      }
    }
    else {
      Left(Error(s"Le requête $url retourne un status ${response.status}"))
    }
  }
}
