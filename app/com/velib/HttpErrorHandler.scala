package com.velib

import scala.concurrent.Future

import com.velib.model.Error
import com.velib.utils.Logging
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc.{ RequestHeader, Result, Results }

class HttpErrorHandler extends play.api.http.HttpErrorHandler with Logging {

  override def onClientError(request: RequestHeader, statusCode: Int, message: String = ""): Future[Result] = {
    logger.warn(s"Something occurs wrong on ${request.path} with method ${request.method} and message($message).")
    statusCode match {
      case BAD_REQUEST =>
        Future.successful(BadRequest(Json.toJson(Error(message))))
      case NOT_FOUND =>
        Future.successful(
          NotFound(Json.toJson(Error(s"Page ${request.path} with ${request.method} not found")))
        )
      case FORBIDDEN => Future.successful(Forbidden(Json.toJson(Error(message))))
      case _ => Future.successful(Results.Status(statusCode)(Json.toJson(Error(message))))
    }
  }

  override def onServerError(request: RequestHeader, exception: Throwable): Future[Result] = {
    logger.error(s"Uncatched error for request: ${request.method} ${request.uri}", exception)
    Future.successful(
      InternalServerError(
        Json.toJson(
          Error(s"Uncatched error for request: ${request.method} ${request.uri}")
          )
        )
    )
  }
}
