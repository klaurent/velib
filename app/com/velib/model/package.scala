package com.velib

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

import play.api.libs.json.{ Format, Reads, Writes }

package object model {
  implicit val format2: Format[ZonedDateTime] = Format(
    Reads.zonedDateTimeReads(DateTimeFormatter.ISO_OFFSET_DATE_TIME),
    Writes.temporalWrites[ZonedDateTime, DateTimeFormatter](
      DateTimeFormatter.ISO_OFFSET_DATE_TIME
    )
  )
}
