package com.velib.model

import java.time.ZonedDateTime

import play.api.libs.json.Json

case class Record(
  recordid: String,
  record_timestamp: ZonedDateTime,
  fields: Field
)

object Record{
  implicit lazy val format = Json.format[Record]
}
