package com.velib.model

import play.api.libs.json.Json

case class VelibData(
  nhits: Int,
  records: List[Record]
)

object VelibData{
  implicit lazy val format = Json.format[VelibData]
}




