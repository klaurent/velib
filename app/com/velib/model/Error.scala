package com.velib.model

import play.api.libs.json.Json

case class Error(
  message: String
)

object Error{
  implicit lazy val format = Json.format[Error]
}
