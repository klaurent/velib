package com.velib.model.data

import play.api.libs.json.Json

case class VelibMax(
  station_name: String,
  value: Int
)

object VelibMax{
  implicit lazy val format = Json.format[VelibMax]
}
