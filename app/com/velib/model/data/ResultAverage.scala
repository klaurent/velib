package com.velib.model.data

import play.api.libs.json.Json

case class ResultAverage(
  station_name: String,
  result: Seq[ResultMoyen]
)

object ResultAverage{
  implicit lazy val format = Json.format[ResultAverage]
}
