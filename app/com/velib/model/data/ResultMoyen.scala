package com.velib.model.data

import play.api.libs.json.Json

case class ResultMoyen(
  hour: Int,
  value: Int
)

object ResultMoyen{
  implicit lazy val format = Json.format[ResultMoyen]
}

