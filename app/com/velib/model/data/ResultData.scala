package com.velib.model.data

import play.api.libs.json.Json

//- Donner le nombre moyen de Vélib' disponibles par station et par heure de la journée pour savoir à quel moment il vaut mieux prendre son Vélib' et dans quelle station.
//- Donner le nombre moyen d'emplacements disponibles dans les stations, par heure de la journée pour savoir à quelle moment il vaut mieux rendre son Vélib' et dans quelle station.
//- Donner le top 3 des stations qui ont le plus de Vélib' disponibles sur la journée.
case class ResultData(
  velibAverage: Seq[ResultAverage],
  dockAverage: Seq[ResultAverage],
  velibMax: Seq[VelibMax]
)

object ResultData{
  implicit lazy val format = Json.format[ResultData]
}
