package com.velib.model

import com.velib.utils.Utils
import play.api.libs.json.Json

case class Field(
  dist: String,
  station_state: String,
  densitylevel: String,
  maxbikeoverflow: Int,
  nbbikeoverflow: Int,
  nbedock: Int,
  kioskstate: String,
  station_name: String,
  nbfreeedock: Int,
  station_type: String,
  station_code: String,
  creditcard: String,
  nbebike: Int,
  duedate: String,
  nbebikeoverflow: Int,
  nbfreedock: Int,
  overflow: String,
  nbdock: Int,
  nbbike: Int,
  geo: List[Double]
) {
  def toLog : String = s"Il ${Utils.pluralize(nbbike, "vélo","y a", "n'y a pas de vélo")} " +
    s"à la station $station_name situé à${Utils.pluralize(dist.toInt, "métre")} " + loc.getOrElse("")

  // test
  def loc : Option[String] = {
    if (geo.length == 2) {
      Some(s" situé https://www.google.com/search?q=${geo.head}%2C+${geo.last}")
    }
    else None
  }
}

object Field{
  implicit lazy val format = Json.format[Field]
}
