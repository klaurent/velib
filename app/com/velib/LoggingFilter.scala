package com.velib

import scala.concurrent.{ ExecutionContext, Future }

import akka.stream.Materializer
import com.velib.utils.Logging
import play.api.mvc.{ Filter, RequestHeader, Result }

class LoggingFilter(implicit val mat: Materializer, ec: ExecutionContext) extends Filter with Logging {
  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    val startTime = System.currentTimeMillis

    nextFilter(requestHeader).map { result =>
      val requestTime = System.currentTimeMillis - startTime

      val affichage: String = s"${requestHeader.method} ${requestHeader.uri} took ${requestTime}ms and returned ${result.header.status}"

      logger.warn(affichage)

      result.withHeaders("Request-Time" -> requestTime.toString)
    }
  }
}
