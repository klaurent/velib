package com.velib.controllers

import scala.concurrent.ExecutionContext

import com.velib.services.OpenDataClient
import play.api.libs.json.Json
import play.api.mvc.{ AbstractController, ControllerComponents }

class ApplicationController (
  openDataClient: OpenDataClient,
  cc: ControllerComponents
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def application = Action.async{_ =>
    openDataClient.findVelibData().map{
      case Left(e) => BadRequest(Json.toJson(e))
      case Right(a) => Ok(Json.toJson(a))
    }
  }

}
