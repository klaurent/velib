package com.velib.controllers

import scala.concurrent.ExecutionContext

import com.velib.data.service.DataService
import play.api.libs.json.Json
import play.api.mvc.{ AbstractController, ControllerComponents }

class DataController (
  dataService: DataService,
  cc: ControllerComponents
)(implicit ec: ExecutionContext) extends AbstractController(cc) {

  def data = Action {
    Ok(Json.toJson(dataService.findData))
  }
}
