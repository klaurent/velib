package com.velib.controllers

import scala.concurrent.ExecutionContext

import play.api.mvc.{ AbstractController, ControllerComponents }

class AdminController(
  cc: ControllerComponents
)(implicit ec: ExecutionContext) extends AbstractController(cc){

  def getFront(path: String) = Action {
    Ok(com.velib.view.html.index())
  }
}
