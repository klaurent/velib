package com.velib.utils

object Utils {
  // add test
  def pluralize(nb: Int, word: String, init: String = "", empty: String = ""): String = {
    if (nb == 0) {
      empty
    } else if (nb == 1) {
      s"$init $nb $word"
    } else {
      s"$init $nb ${word}s"
    }
  }
}
