package com.velib.utils

import org.slf4j.LoggerFactory

trait Logging {
  val logger = {
    val name = this.getClass.getName.stripSuffix("$")
    LoggerFactory.getLogger(name)
  }
}
