package com.velib.task

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import akka.actor.{ ActorSystem, Cancellable }
import com.velib.services.OpenDataClient
import com.velib.utils.Logging

class VelibDataTask(
  actorSystem: ActorSystem,
  openDataClient: OpenDataClient
)(implicit executionContext: ExecutionContext) extends Logging {

  def task(): Cancellable = {
    actorSystem.scheduler.schedule(initialDelay = 2.seconds, interval = 1.minute) {
      openDataClient.findVelibData().map {
        case Left(_) => ()
        case Right(a) => a.records.foreach { r =>
          logger.warn(r.fields.toLog)
        }
      }
    }
  }

}
