package com.velib.data.service

import scala.collection.mutable
import scala.io.Source

import com.velib.data.store.DataStore
import com.velib.model.data.{ VelibMax, ResultData, ResultMoyen, ResultAverage }
import com.velib.model.{ Record, VelibData, data }
import com.velib.utils.Logging
import play.api.libs.json.{ JsError, JsSuccess, Json }

class DataService(
  dataStore: DataStore
) extends Logging{

  private [velib] def loadAllDataFromFile() = {
    for (line <- Source.fromURL(
      getClass.getClassLoader.getResource("data/velib_dataset_c271c5d8-6b77-4557-845c-3b449863bbb0.txt")
    ).getLines) {
      Json.parse(line).validate[VelibData] match {
        case JsError(errors) => logger.error(s"Impossible de décoder $line à cause de l'erreur $errors")
        case JsSuccess(value, _) => {
          dataStore.add(value)
        }
      }
    }
    dataStore.getAll
  }

  def getAll(): mutable.Map[String, mutable.Map[Int, Seq[Record]]] = dataStore.getAll


  //- Donner le nombre moyen de Vélib' disponibles par station et par heure de la journée pour savoir à quel moment il vaut mieux prendre son Vélib' et dans quelle station.
  //- Donner le nombre moyen d'emplacements disponibles dans les stations, par heure de la journée pour savoir à quelle moment il vaut mieux rendre son Vélib' et dans quelle station.
  //- Donner le top 3 des stations qui ont le plus de Vélib' disponibles sur la journée.
  def findData = {
    val datas = dataStore.getAll
    ResultData(
      findVelib(datas),
      findDock(datas),
      top3(datas),
    )
  }

  private[service] def findVelib(datas: mutable.Map[String, mutable.Map[Int, Seq[Record]]]): Seq[ResultAverage] = {
    datas.map{
      case(key, value) => {
        ResultAverage(
          station_name = key,
          result = value.map { case (hour, values) =>
            val nb = if (values.isEmpty) -1 else values.map(_.fields.nbbike).sum / values.length
            ResultMoyen(
              hour = hour, value = nb
            )
          }.toSeq.sortBy(_.hour)
        )
      }
    }.toSeq
  }

  private[service] def findDock(datas: mutable.Map[String, mutable.Map[Int, Seq[Record]]]): Seq[ResultAverage] = {
    datas.map{
      case(key, value) => {
        ResultAverage(
          station_name = key,
          result = value.map { case (hour, values) =>
            val nb = if (values.isEmpty) -1 else values.map(_.fields.nbfreeedock).sum / values.length
            ResultMoyen(
              hour = hour, value = nb
            )
          }.toSeq.sortBy(_.hour)
        )
      }
    }.toSeq
  }

  private[service] def top3(datas: mutable.Map[String, mutable.Map[Int, Seq[Record]]]): Seq[VelibMax] = datas.map{
    case(key, value) => {
      VelibMax(
        station_name = key,
        value = value.values.flatten.map(_.fields.nbbike).sum
      )
    }
  }.toSeq.sortBy(-_.value).take(3)
}
