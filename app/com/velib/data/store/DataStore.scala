package com.velib.data.store

import scala.collection.mutable

import com.velib.model.{ Record, VelibData }
import javax.inject.Singleton

@Singleton
class DataStore {
  private val velibDatas : mutable.Map[String,mutable.Map[Int,Seq[Record]]] = mutable.Map.empty[String,mutable.Map[Int,Seq[Record]]]

  def add(velibData: VelibData): Unit = {
    velibData.records.foreach{ record =>
      val station_name = record.fields.station_name
      velibDatas.get(station_name) match {
        case None => {
          val initMap = mutable.Map.empty[Int,Seq[Record]]
          initMap.put(record.record_timestamp.getHour, Seq(record))
          velibDatas.put(station_name, initMap)
        }
        case Some(value) => {
          val keyHour = record.record_timestamp.getHour
          value.get(keyHour) match {
            case None => {
              value.put(keyHour, Seq(record))
              velibDatas.update(station_name, value)
            }
            case Some(records) => {
              value.update(keyHour, records :+ record)
              velibDatas.update(station_name, value)
            }
          }
        }
      }
    }
  }

  def getAll: mutable.Map[String, mutable.Map[Int, Seq[Record]]] = velibDatas

}
