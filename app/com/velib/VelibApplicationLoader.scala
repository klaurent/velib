package com.velib

import scala.concurrent.Future

import com.velib.controllers.{ AdminController, ApplicationController, DataController }
import com.velib.data.service.DataService
import com.velib.data.store.DataStore
import com.velib.services.OpenDataClient
import com.velib.task.VelibDataTask
import play.api._
import play.api.libs.ws.ahc.AhcWSComponents
import play.api.mvc.EssentialFilter
import play.api.routing.Router
import play.filters.cors.{ CORSConfig, CORSFilter }
import router.Routes

class VelibApplicationLoader extends ApplicationLoader {
  def load(context: ApplicationLoader.Context) = {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment)
    }
    new EnchereComponents(context).application
  }
}

class EnchereComponents(context: ApplicationLoader.Context)
  extends BuiltInComponentsFromContext(context)
    with _root_.controllers.AssetsComponents
    with AhcWSComponents {

  val defaultEc = controllerComponents.executionContext

  override lazy val httpErrorHandler = new HttpErrorHandler()

  val openDataClient = new OpenDataClient(wsClient, configuration)

  val dataService = new DataService(new DataStore)
  dataService.loadAllDataFromFile()

  val applicationController = new ApplicationController(openDataClient, controllerComponents)
  val dataController = new DataController(dataService, controllerComponents)
  val adminController = new AdminController(controllerComponents)

  def router: Router = new Routes(httpErrorHandler, applicationController, dataController, assets, adminController)

  val manualTask = new VelibDataTask(actorSystem, openDataClient).task()

  override lazy val httpFilters: Seq[EssentialFilter] = Seq(
    CORSFilter(CORSConfig.fromConfiguration(configuration), httpErrorHandler),
    new LoggingFilter()
  )

  applicationLifecycle.addStopHook(() => {
    Future {
      manualTask.cancel()
    }
  })
}
