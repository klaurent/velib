# Velib

Axes d'améliorations:
- Mieux séparer les modules:
  - Un dossier server
  - Un dossier client
- Séparer l'application back en 3 modules
  - module contenant les modéles commun
  - module data
  - module application
- Pour la partie data, retourner les résultats trier station en fonction de la distance par rapport à l'adresse
- Pour la partie data utiliser NonEmptySeq (pour ne pas faire head ni last)
- Pour la partie Front, mutualiser exo1, exo2, exo3
- Pour la partie Front, rendre le front plus attrayant visuellement
- Pour la partie front, enelevr les warning sur les key

#Lancement:
Lancement du front
- `cd client`
- `yarn install`
- `yarn run build`

Lancement de l'API:
- `sbt run`
- lancement des tests : `sbt test`

#L'affichage des résultats se fait sur :
- Pour la partie application `http://localhost:9000/application`
- Pour la partie data exercice 1 `http://localhost:9000/data/1`
- Pour la partie data exercice 2 `http://localhost:9000/data/2`
- Pour la partie data exercice 3 `http://localhost:9000/data/3`

# Explication partie data
La partie Data se fait dans le service DataService.
Nous montons toutes les données en mémoire (étant donné que l'on a un petit volume de donné, si on en avait plus on aurai peut être opté pour une base document).
Les données en mémoire sont de la forme `Map[NomStation, Map[Heure, Seq[Records]]]`.
Cela nous permet de faire trés facilement les différentes opération.
