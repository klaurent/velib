package com.velib.utils

import org.scalatest.{ Matchers, WordSpec }

class UtilsSpec extends WordSpec with Matchers {
  "UtilsSpec validator" should {
    "success with no values" in {
      val result = Utils.pluralize(0, "test", "", "empty")

      result shouldBe("empty")
    }

    "success with on value" in {
      val result = Utils.pluralize(1, "test")

      result shouldBe(" 1 test")
    }

    "success with few values" in {
      val result = Utils.pluralize(2, "test")

      result shouldBe(" 2 tests")
    }
  }
}
