package com.velib.model

import java.time.ZonedDateTime

trait RecordFixture {

  def record(nbBike: Int = 1, nbfreeedock: Int = 1) = Record(
    "",
    ZonedDateTime.parse("1989-05-19T18:59:47Z"),
    fields = field(nbBike, nbfreeedock)
  )


  def field(nbBike: Int = 1, nbfreeedock: Int = 1) = Field(
    dist = "dist ",
    station_state = "station_state ",
    densitylevel = "densitylevel ",
    maxbikeoverflow = 0,
    nbbikeoverflow = 0,
    nbedock = 0,
    kioskstate = "kioskstate ",
    station_name = "station_name ",
    nbfreeedock = nbfreeedock,
    station_type = "station_type ",
    station_code = "station_code ",
    creditcard = "creditcard ",
    nbebike = 0,
    duedate = "duedate ",
    nbebikeoverflow = 0,
    nbfreedock = 0,
    overflow = "overflow ",
    nbdock = 0,
    nbbike = nbBike,
    geo = Nil
  )

}
