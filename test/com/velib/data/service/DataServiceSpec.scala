package com.velib.data.service

import scala.collection.mutable

import com.velib.data.store.DataStore
import com.velib.model.RecordFixture
import com.velib.model.data.ResultMoyen
import org.scalatest.{ Matchers, WordSpec }

class DataServiceSpec extends WordSpec with Matchers {
  "DataService" should {
    val dataService = new DataService(new DataStore())
    "success to findVelib" in new RecordFixture {
      val mapValues = mutable.Map(
        "1" -> mutable.Map(0 -> Seq(record(2))),
        "2" -> mutable.Map(
          0 -> Seq(record(2), record(4)),
          1 -> Seq(record(1))
        ),
      )

      val result = dataService.findVelib(mapValues)

      result.find(_.station_name == "1").get.result should contain oneElementOf Seq(ResultMoyen(0, 2))
      result.find(_.station_name == "2").get.result should contain allElementsOf Seq(ResultMoyen(0, 3), ResultMoyen(1, 1))
    }

    "success to findDock" in new RecordFixture {
      val mapValues = mutable.Map(
        "1" -> mutable.Map(0 -> Seq(record(2, 2))),
        "2" -> mutable.Map(
          0 -> Seq(record(2, 2), record(4, 4)),
          1 -> Seq(record(1, 1))
        ),
      )

      val result = dataService.findDock(mapValues)

      result.find(_.station_name == "1").get.result should contain oneElementOf Seq(ResultMoyen(0, 2))
      result.find(_.station_name == "2").get.result should contain allElementsOf Seq(ResultMoyen(0, 3), ResultMoyen(1, 1))
    }


    "success to top3" in new RecordFixture {
      val mapValues = mutable.Map(
        "1" -> mutable.Map(0 -> Seq(record(3, 2))),
        "2" -> mutable.Map(0 -> Seq(record(4, 2))),
        "3" -> mutable.Map(0 -> Seq(record(2, 2))),
        "4" -> mutable.Map(0 -> Seq(record(2, 2))),
        "5" -> mutable.Map(
          0 -> Seq(record(2, 2), record(4, 4)),
          1 -> Seq(record(1, 1))
        ),
      )

      val result = dataService.top3(mapValues)

      result.head.value shouldBe 7
      result.find(_.station_name == "1").get.value shouldBe 3
      result.find(_.station_name == "2").get.value shouldBe 4
    }
  }
}
