import { ResultMoyen } from './ResultMoyen'

export interface ResultAverage {
  station_name: string,
  result: Array<ResultMoyen>
}
