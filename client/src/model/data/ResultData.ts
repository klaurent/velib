import { ResultAverage } from './ResultAverage'
import { VelibMax } from './VelibMax'

export interface ResultData {
  velibAverage: Array<ResultAverage>
  dockAverage: Array<ResultAverage>
  velibMax: Array<VelibMax>
}
