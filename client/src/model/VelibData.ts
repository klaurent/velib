import { Record } from './Record'

export interface VelibData {
  nhits: number
  records: Array<Record>
}
