import { Fields } from './Fields'

export interface Record {
  recordid: string
  record_timestamp: string
  fields: Fields
}
