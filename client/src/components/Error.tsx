import React from 'react'

export const Error = () => (
  <div className="alert alert-danger" role="alert">
    <strong>Désolé</strong> La page désiré n'existe pas.
  </div>
)
