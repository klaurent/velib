import React from 'react'
import { AverageLineComponent } from './AverageLineComponent'
import { ResultData } from '../model/data/ResultData'

interface Store {
  data?: ResultData
}


export class Exo2 extends React.Component<{}, Store> {
  constructor(props: {}) {
    super(props);

    this.state = {
      data: undefined
    }

  }


  componentWillMount() {
    fetch("http://localhost:9000/api/data")
      .then( result => result.json())
      .then( (resp: ResultData)  => {
        this.setState({
          data: resp
        })
      })
  }

  render() {
    const { data } = this.state

    if (data) {
      return <div>
        <div className="container theme-showcase" role="main">
          <table className="table table-striped">
            <thead>
            <tr>
              <th> Nom de la station </th>
              <th> Heure </th>
              <th> Nombre de dock libre </th>
            </tr>
            </thead>
            <tbody>
            {data.dockAverage.map( (r) => (
                <AverageLineComponent average={r} key={r.station_name + r.result.toString()} />
              )
            )}
            </tbody>
          </table>
        </div>
      </div>
    } else {
      return <div className="alert alert-info" role="alert">
        Chargement...
      </div>
    }

  }
}
