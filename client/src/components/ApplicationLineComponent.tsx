import * as React from 'react'
import { Fields } from '../model/Fields'

interface Props {
  field: Fields
}

export class ApplicationLineComponent extends React.Component<Props, {}> {

  render() {
    const { field } = this.props
    return (
      <tr>
        <td> { field.station_name } </td>
        <td> { field.nbbike } </td>
        <td> { field.nbfreeedock } </td>
        <td> { field.dist } </td>
        <td> <a href={"https://www.google.com/search?q="+field.geo[0]+"%2C+"+field.geo[1]}> Lien google </a> </td>
      </tr>
    )
  }
}
