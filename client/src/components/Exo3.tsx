import React from 'react'
import { ResultData } from '../model/data/ResultData'

interface Store {
  data?: ResultData
}


export class Exo3 extends React.Component<{}, Store> {
  constructor(props: {}) {
    super(props);

    this.state = {
      data: undefined
    }

  }


  componentWillMount() {
    fetch("http://localhost:9000/api/data")
      .then( result => result.json())
      .then( (resp: ResultData)  => {
        this.setState({
          data: resp
        })
      })
  }

  render() {
    const { data } = this.state

    if (data) {
      return <div>
        <div className="container theme-showcase" role="main">
          <table className="table table-striped">
            <thead>
            <tr>
              <th> Nom de la station </th>
              <th> Nombre total de vélib </th>
            </tr>
            </thead>
            <tbody>
            {data.velibMax.map( (r) => (
              <tr>
               <td>{r.station_name}</td>
               <td>{r.value}</td>
              </tr>
              )
            )}
            </tbody>
          </table>
        </div>
      </div>
    } else {
      return <div className="alert alert-info" role="alert">
        Chargement...
      </div>
    }

  }
}
