import React from 'react'
import { ApplicationLineComponent } from './ApplicationLineComponent'
import { VelibData } from '../model/VelibData'

interface Store {
  data?: VelibData
  intervalID?: any
}


export class Application extends React.Component<{}, Store> {
  constructor(props: {}) {
    super(props);

    this.state = {
      data: undefined,
      intervalID: setTimeout(this.getData.bind(this), 60000)
    }

    this.getData()
  }

  componentWillUnmount() {
    clearTimeout(this.state.intervalID);
  }

  getData = () => {
    fetch("http://localhost:9000/api/application")      // returns a promise object
      .then( result => result.json()) // still returns a promise object, U need to chain it again
      .then( (resp: VelibData)  => {
        this.setState({ data: resp , intervalID:this.state.intervalID});
      })
  }

  render() {
    const { data } = this.state

    if (data) {
      return (
        <div className="container theme-showcase" role="main">
          <table className="table table-striped">
            <thead>
            <tr>
              <th> Nom de la station </th>
              <th> Nombre de vélo </th>
              <th> Nombre de dock libre </th>
              <th> Distance </th>
              <th> Lien google </th>
            </tr>
            </thead>
            <tbody>
            {data.records.map( (r) => (
                <ApplicationLineComponent field={r.fields} key={r.fields.station_code.toString()} />
              )
            )}
            </tbody>
          </table>
        </div>)
    } else {
      return <div className="alert alert-info" role="alert">
        Chargement...
      </div>
    }

  }
}
