import * as React from 'react'
import { ResultAverage } from '../model/data/ResultAverage'

interface Props {
  average: ResultAverage
}

export class AverageLineComponent extends React.Component<Props, {}> {

  render() {
    const { average } = this.props
    return (<>
      {
        average.result.map( (r) =>
          <tr>
            <td> { average.station_name } </td>
            <td> { r.hour } </td>
            <td> { r.value } </td>
          </tr>
        )
      }
      </>
    )
  }
}
