import React from 'react'
import { Route, Switch } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { Exo1 } from './components/Exo1'
import { Exo2 } from './components/Exo2'
import { Exo3 } from './components/Exo3'
import { Application } from './components/Application'
import { Error } from './components/Error'


class App extends React.Component<{}, {}> {
  render() {
    return <BrowserRouter>
        <Switch >
          <Route exact path="/application" component={ Application } />
          <Route exact path="/data/1" component={ Exo1 } />
          <Route exact path="/data/2" component={ Exo2 } />
          <Route exact path="/data/3" component={ Exo3 } />
          <Route component={Error} />
        </Switch>
      </BrowserRouter>
  }
}

export default App
